<?php
header("Access-Control-Allow-Origin: *");

if ($_POST['key'] === 'watermelonjuice') {
    file_put_contents("total.txt", $_POST['total']);

    if ($_POST['donator'] === 'true') {
        date_default_timezone_set('America/New_York');
        $record = date("M d Y h:i:sa") . "\tAmount donated: " .$_POST['amount'] . "\tUpdated total is\t" . $_POST['total'] . PHP_EOL;
        file_put_contents("record.txt", $record, FILE_APPEND);
    }

}

echo file_get_contents("total.txt");
